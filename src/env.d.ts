declare namespace NodeJS {
  interface ProcessEnv {
    NODE_URL: string;
    NODE_PORT: string;
  }
}
