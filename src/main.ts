import { Logger } from '@nestjs/common';
import { NestFactory } from '@nestjs/core';
import { AppModule } from './app.module';

async function bootstrap() {
  const app = await NestFactory.create(AppModule);
  Logger.verbose(
    `${process.env.NODE_URL}:${process.env.NODE_PORT}`,
    'main.ts > 서버시작 🧭 ',
  );

  await app.listen(process.env.NODE_PORT);
}
bootstrap();
